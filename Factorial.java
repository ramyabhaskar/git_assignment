// This is the first comment made by Ramya Bhaskar. This is a file.
// This is the second comment made by Ramya Bhaskar. We are beginning a class def.
//Code to calculate factorial of a number
import java.util.Scanner;
//The main class 
class Factorial
{
   public static void main(String args[])
   {
      int n, c, fact = 1;
 
      System.out.println("Enter an integer to calculate it's factorial");
      //Comment to conflict taking inputs
      Scanner in = new Scanner(System.in);
 
      n = in.nextInt();
 // This is the third comment made by Ramya Bhaskar. we are setting n's value.
 //Test end case
      if ( n < 0 )
         System.out.println("Number should be non-negative.");
      else
      {
// This is the fourth comment made by Ramya Bhaskar, we are starting a for loop.
//Actual logic for calculating factorial
         for ( c = 1 ; c <= n ; c++ )
            fact = fact*c;
 
         System.out.println("Factorial of "+n+" is = "+fact);
      }
   }
}
